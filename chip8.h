/*
** chip8.h
** Friedrich Hölzel (Herbert M.)
**
** holds the  chip8 class
*/

#include <iostream>
#include <string>
#include <fstream>
#include <iomanip>
#include <ctime>
#include <cstdlib>
using std::fstream;
using std::ios;
using std::string;
using std::cout;
using std::hex;
using std::setbase;

class chip8 {
    //cpu stuff
    uint8_t V[16];
    uint16_t I;
    uint16_t stack[16];
    uint16_t SP;
    uint16_t PC;
    uint16_t delay;
    uint16_t sound;
    unsigned char memory[4096];
    uint16_t font[80] = {
        0xF0, 0x90, 0x90, 0x90, 0xF0, // 0
        0x20, 0x60, 0x20, 0x20, 0x70, // 1
        0xF0, 0x10, 0xF0, 0x80, 0xF0, // 2
        0xF0, 0x10, 0xF0, 0x10, 0xF0, // 3
        0x90, 0x90, 0xF0, 0x10, 0x10, // 4
        0xF0, 0x80, 0xF0, 0x10, 0xF0, // 5
        0xF0, 0x80, 0xF0, 0x90, 0xF0, // 6
        0xF0, 0x10, 0x20, 0x40, 0x40, // 7
        0xF0, 0x90, 0xF0, 0x90, 0xF0, // 8
        0xF0, 0x90, 0xF0, 0x10, 0xF0, // 9
        0xF0, 0x90, 0xF0, 0x90, 0x90, // A
        0xE0, 0x90, 0xE0, 0x90, 0xE0, // B
        0xF0, 0x80, 0x80, 0x80, 0xF0, // C
        0xE0, 0x90, 0x90, 0x90, 0xE0, // D
        0xF0, 0x80, 0xF0, 0x80, 0xF0, // E
        0xF0, 0x80, 0xF0, 0x80, 0x80  // F

    };
    uint16_t disp[64][32];
    uint16_t oldDisp[64][32];
    bool drawf;
    uint16_t keys[16];

    public:
        chip8();
        uint16_t getV(uint16_t x);
        uint16_t getI();
        uint16_t getStack(uint16_t x);
        uint16_t getSP();
        uint16_t getPC();
        uint16_t getDelay();
        uint16_t getSound();
        unsigned char getMemory(int pc);
        uint16_t getDisp(uint16_t j, uint16_t i);
        uint16_t getOldDisp(uint16_t j, uint16_t i);
        bool getDrawf();
        uint16_t getKey(uint16_t key);
        ~chip8();
        //setters
        void init();
        void setV(uint16_t val, uint16_t element);
        void setI(uint16_t i);
        void setStack(uint16_t val, uint16_t element);
        void setSP(uint16_t sp);
        void setPC(uint16_t pc);
        void incrementPC();
        void decrementPC();
        void setDelay(uint16_t d);
        void setSound(uint16_t s);
        void decrementDelay();
        void decrementSound();
        void setMemory(unsigned char val, uint16_t element);
        void loadRom(string rom);
        void setDisp(uint16_t j, uint16_t i, uint16_t x);
        void setOldDisp(uint16_t j, uint16_t i, uint16_t x);
        void setDrawf(bool dr);
        void setKey(uint16_t val, uint16_t key);
        void emulate();
        //instructions
        void op0x00(uint16_t op);
        void op0x00e0(uint16_t op);
        void op0x00ee(uint16_t op);
        void op0x01(uint16_t op);
        void op0x02(uint16_t op);
        void op0x03(uint16_t op);
        void op0x04(uint16_t op);
        void op0x05(uint16_t op);
        void op0x06(uint16_t op);
        void op0x07(uint16_t op);
        void op0x08(uint16_t op);
        void op0x080(uint16_t op);
        void op0x081(uint16_t op);
        void op0x082(uint16_t op);
        void op0x083(uint16_t op);
        void op0x084(uint16_t op);
        void op0x085(uint16_t op);
        void op0x086(uint16_t op);
        void op0x087(uint16_t op);
        void op0x08e(uint16_t op);
        void op0x09(uint16_t op);
        void op0x0a(uint16_t op);
        void op0x0b(uint16_t op);
        void op0x0c(uint16_t op);
        void op0x0d(uint16_t op);
        void op0x0e(uint16_t op);
        void op0x0e9(uint16_t op);
        void op0x0ea(uint16_t op);
        void op0x0f(uint16_t op);
        void op0x0f07(uint16_t op);
        void op0x0f0A(uint16_t op);
        void op0x0f15(uint16_t op);
        void op0x0f18(uint16_t op);
        void op0x0f1E(uint16_t op);
        void op0x0f29(uint16_t op);
        void op0x0f33(uint16_t op);
        void op0x0f55(uint16_t op);
        void op0x0f65(uint16_t op);


};
