/*
** chip8.cpp
** Friedrich Hölzel (Herbert M.)
**
** implementation of chip8
*/

#include "chip8.h"

chip8::chip8() {
    init();
}

uint16_t chip8::getV(uint16_t x) { return V[x]; }
uint16_t chip8::getI() { return I; }
uint16_t chip8::getStack(uint16_t x) { return stack[x]; }
uint16_t chip8::getSP() { return SP; }
uint16_t chip8::getPC() { return PC; }
uint16_t chip8::getDelay() { return delay; }
uint16_t chip8::getSound() { return sound; }
unsigned char chip8::getMemory(int pc) { return memory[pc]; }
uint16_t chip8::getDisp(uint16_t j, uint16_t i) { return disp[j][i]; }
uint16_t chip8::getOldDisp(uint16_t j, uint16_t i) { return oldDisp[j][i]; }
bool chip8::getDrawf() { return drawf; }
uint16_t chip8::getKey(uint16_t key) { return keys[key]; }
chip8::~chip8() { }

//setters
void chip8::init() {
    setSP(0xFA0);
    setI(0);
    setDelay(0);
    setSound(0);
    for(int x=0;x<16;x++){
        setStack(0,x);
    }
    setPC(0x200);
    for(int l=0;l<80;l++) {
        memory[l] = font[l];
    }
    for(int f=0;f<64;f++){
        for(int u=0;u<32;u++){
            setDisp(f, u, 0);
        }
    }
    setDrawf(false);
    for(int i=0;i<16;i++) {
        setKey(0,i);
    }
}
void chip8::setV(uint16_t val, uint16_t element) {
    V[element] = val;
}
void chip8::setI(uint16_t f) {
    I = f;
}
void chip8::setStack(uint16_t val, uint16_t element) {
    stack[element] = val;
}
void chip8::setSP(uint16_t p) {
    SP = p;
}
void chip8::setPC(uint16_t xe) {
    PC = xe;
}
void chip8::incrementPC() {
    PC += 2;
}
void chip8::decrementPC() {
    PC -= 2;
}
void chip8::setDelay(uint16_t d) {
    delay = d;
}
void chip8::setSound(uint16_t s) {
    sound = s;
}
void chip8::decrementDelay() { delay--; }
void chip8::decrementSound() { sound--; }
void chip8::setMemory(unsigned char val, uint16_t element) {
    memory[element] = val;
}
void chip8::loadRom(string rom) {
    //open file for reading
    fstream file (rom, ios::in | ios::binary | ios::out);

    if(!file) {
        cout << "Error couldn't open file!" << '\n';
        exit(1);
    }

    //get file size
    file.seekg(0L, ios::end);
    long int filesize = file.tellg();
    file.seekg(0L, ios::beg); //set pointer back to start

    //read file into memory at 0x200 and close it
    file.read((char*)memory+0x200, filesize);
    file.close();

}
void chip8::setDisp(uint16_t j, uint16_t i, uint16_t x) {
    disp[j][i] = x;
}
void chip8::setOldDisp(uint16_t j, uint16_t i, uint16_t x) {
    oldDisp[j][i] = x;
}
void chip8::setDrawf(bool dr) {
    drawf = dr;
}
void chip8::setKey(uint16_t val, uint16_t key) {
    keys[key] = val;
}

void chip8::emulate() {
    //gotta emulate
    uint16_t code = getMemory(getPC()) << 8 | getMemory(getPC() + 1);
    cout << setbase(16) << "opcode: " << (int)code << ' ' << "PC: " << PC << '\n';
    uint16_t opcode = code & 0xF000;

    switch(opcode) {
        case 0x0000: op0x00(code); break;
        case 0x1000: op0x01(code); break;
        case 0x2000: op0x02(code); break;
        case 0x3000: op0x03(code); break;
        case 0x4000: op0x04(code); break;
        case 0x5000: op0x05(code); break;
        case 0x6000: op0x06(code); break;
        case 0x7000: op0x07(code); break;
        case 0x8000: op0x08(code); break;
        case 0x9000: op0x09(code); break;
        case 0xa000: op0x0a(code); break;
        case 0xb000: op0x0b(code); break;
        case 0xc000: op0x0c(code); break;
        case 0xd000: op0x0d(code); break;
        case 0xe000: op0x0e(code); break;
        case 0xf000: op0x0f(code); break;
        default: cout << "default\n"; break;

    }

}

//instructions
void chip8::op0x00(uint16_t op) {
    switch(op) {
        case 0x00e0: op0x00e0(op); break;
        case 0x00ee: op0x00ee(op); break;
        default: incrementPC(); cout << "invalid 0x00 " << (int)op << '\n';
    }
}
void chip8::op0x00e0(uint16_t op) {
    //clear the screen
    for(int j=0;j<64;j++) {
        for(int i=0;i<32;i++) {
            setDisp(j, i, 0);
        }
    }
    setDrawf(true);
    incrementPC();
}
void chip8::op0x00ee(uint16_t op) {
    // return from a subroutine
    uint16_t decSP = getSP() - 1;
    setSP(decSP);
    uint16_t addr = getStack(getSP());
    setPC(addr);
    incrementPC();
}
void chip8::op0x01(uint16_t op) {
    //jump to nnn
    uint16_t addr = op & 0x0FFF;
    if(getPC() == addr) {
        cout << "Error trying to hault!" << '\n';
        exit(1);
    }
    setPC(addr);
}
void chip8::op0x02(uint16_t op) {
    // call subroutine at nnn
    uint16_t currentPC = getPC();
    uint16_t currentSP = getSP();
    setStack(currentPC, currentSP);

    setSP(currentSP + 1);
    uint16_t nnn = op & 0x0FFF;
    setPC(nnn);
}
void chip8::op0x03(uint16_t op) {
    //skip next instruction if Vx = kk
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    uint16_t kk = (op & 0xFF);
    if(Vx == kk) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x04(uint16_t op) {
    // skip next instruction if Vx != kk
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    uint16_t kk = (op & 0xFF);
    if(Vx != kk) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x05(uint16_t op) {
    // skip instruction if Vx = Vy
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    if(Vx == Vy) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x06(uint16_t op) {
    //put value kk into Vx
    uint16_t Vx = (op & 0x0F00) >> 8;
    uint16_t kk = (op & 0xFF);
    setV(kk, Vx);
    incrementPC();
}
void chip8::op0x07(uint16_t op) {
    //adds kk to the value of register Vx

    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t kk = op & 0xFF;
    uint16_t oldVx = getV(x);

    setV(kk+oldVx, x);

    incrementPC();
}
void chip8::op0x08(uint16_t op) {
    // decide what instruction to call depending on the last 4 bits
    uint16_t lastBits = op & 0xF;
    switch(lastBits) {
        case 0x0: op0x080(op); break;
        case 0x1: op0x081(op); break;
        case 0x2: op0x082(op); break;
        case 0x3: op0x083(op); break;
        case 0x4: op0x084(op); break;
        case 0x5: op0x085(op); break;
        case 0x6: op0x086(op); break;
        case 0x7: op0x087(op); break;
        case 0xE: op0x08e(op); break;
    }
}
void chip8::op0x080(uint16_t op) {
    // set Vx to Vy
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vy = getV(y);
    setV(Vy, x);
    incrementPC();
}
void chip8::op0x081(uint16_t op) {
    // set Vx = Vx OR Vy
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    setV((Vx | Vy), x);
    incrementPC();
}
void chip8::op0x082(uint16_t op) {
    //set Vx = Vx AND Vy
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    setV((Vx & Vy), x);
    incrementPC();
}
void chip8::op0x083(uint16_t op) {
    //set Vx = Vx XOR Vy
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    setV((Vx ^ Vy), x);
    incrementPC();
}
void chip8::op0x084(uint16_t op) {
    // set Vx = Vx + Vy, set VF = carry
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    if((Vx + Vy) > 0xFF) {
        //set VF = 1 if sum is greater than 8bits
        setV(1,0xF);
    } else {
        setV(0,0xF);
    }
    uint16_t result = (Vx +Vy) & 0xFF;
    setV(result,x);
    incrementPC();

}
void chip8::op0x085(uint16_t op) {
    // set Vx = Vx - Vy, set VF = NOT borrow
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    if(Vx > Vy) {
        setV(1, 0xF);
    } else {
        setV(0, 0xF);
    }
    uint16_t result = Vx - Vy;
    setV(result, x);
    incrementPC();
}
void chip8::op0x086(uint16_t op) {
    // set Vx = Vx SHR 1
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t Vx = getV(x);
    if((Vx & 0x1) == 1) {
        setV(1, 0xF);
    } else {
        setV(0, 0xF);
    }
    setV((Vx / 2), x);
    incrementPC();

}
void chip8::op0x087(uint16_t op) {
    // set Vx = Vy - Vx, set VF = NOT Borrow
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    if(Vy > Vx) {
        setV(1, 0xF);
    } else {
        setV(0, 0xF);
    }
    setV((Vy - Vx), x);
    incrementPC();
}
void chip8::op0x08e(uint16_t op) {
    // Vx = Vx SHL 1
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t Vx = getV(x);
    if(((Vx & 0x80) >> 0x7) == 1) {
        setV(1, 0xF);
    } else {
        setV(0, 0xF);
    }
    setV((Vx * 2), x);
    incrementPC();
}

void chip8::op0x09(uint16_t op) {
    // skip next instruction if  Vx != Vy
    uint16_t x = (op & 0x0FFF) >> 8;
    uint16_t y = (op & 0xF0) >> 4;
    uint16_t Vx = getV(x);
    uint16_t Vy = getV(y);
    if(Vx != Vy) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x0a(uint16_t op) {
    //set I = nnn
    uint16_t nnn = op & 0x0FFF;
    setI(nnn);
    incrementPC();
}
void chip8::op0x0b(uint16_t op) {
    // jump to location nnn + V0
    uint16_t nnn = op & 0x0FFF;
    uint16_t V0 = getV(0x0);
    setPC(nnn + V0);
}
void chip8::op0x0c(uint16_t op) {
    // set Vx = random byte AND kk
    srand(time(NULL)); //random seed
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t kk = op & 0xFF;

    uint16_t rbyte = (rand() % 256 + 1) & kk;
    setV(rbyte, x);
    incrementPC();
}
void chip8::op0x0d(uint16_t op) {
    //display n byte sprite starting at mem location I at (Vx,Vy), set VF = collision
    uint16_t x = getV((op & 0x0F00) >> 8);
    uint16_t y = getV((op & 0xF0) >> 4);
    uint16_t n = op & 0xF;

    setV(0,0xF);

    //set old display
    for(int f=0;f<64;f++){
        for(int u=0;u<32;u++){
            setOldDisp(f, u, getDisp(f, u));
        }
    }
    //setting display
    for(int i=0;i<n;i++) {
        uint16_t sprite = getMemory(getI() + i);
        for(int p=0;p<8;p++) {
            if(((sprite & (0x80 >> p)) != 0)) {
                if((getDisp(x+p,y+i) !=0)) {
                    setV(1,0xF);
                }
                disp[x+p][y+i] ^= 1;
            }
        }
    }
    setDrawf(true);
    incrementPC();
}
void chip8::op0x0e(uint16_t op) {
    uint16_t lastbits = op & 0xFF;
    switch(lastbits) {
        case 0x9e: op0x0e9(op); break;
        case 0xa1: op0x0ea(op); break;
        default: cout << "not expected lastbit in 0e instruction" << '\n'; break;
    }
}
void chip8::op0x0e9(uint16_t op) {
    // skip next instruction if key with value of Vx is pressed
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    if(getKey(Vx) == 1) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x0ea(uint16_t op) {
    // skip next instruction if key with value of Vx is not pressed
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    if(getKey(Vx) == 0) {
        incrementPC();
        incrementPC();
    } else {
        incrementPC();
    }
}
void chip8::op0x0f(uint16_t op) {
    uint16_t lastbits = op & 0xFF;
    switch(lastbits) {
        case 0x07: op0x0f07(op); break;
        case 0x0A: op0x0f0A(op); break;
        case 0x15: op0x0f15(op); break;
        case 0x18: op0x0f18(op); break;
        case 0x1E: op0x0f1E(op); break;
        case 0x29: op0x0f29(op); break;
        case 0x33: op0x0f33(op); break;
        case 0x55: op0x0f55(op); break;
        case 0x65: op0x0f65(op); break;
    }
}
void chip8::op0x0f07(uint16_t op) {
    // set Vx = delay timer value
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t dt = getDelay();
    setV(dt, x);
    incrementPC();
}
void chip8::op0x0f0A(uint16_t op) {
    // wait for a keypress, store the value of the key in Vx
    bool chec = false;
    uint16_t x = (op & 0x0F00) >> 8;
    for(int i=0;i<16;i++) {
        if(getKey(i) != 0) {
            setV(i,x);
            chec = true;
        }
    }
    if(chec == false) {
        decrementPC();
    }
    incrementPC();
}
void chip8::op0x0f15(uint16_t op) {
    //set dt = Vx
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    setDelay(Vx);
    incrementPC();
}
void chip8::op0x0f18(uint16_t op) {
    //set st = Vx
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    setSound(Vx);
    incrementPC();
}
void chip8::op0x0f1E(uint16_t op) {
    // set I = I + Vx
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    uint16_t oldI = getI();
    setI(Vx + oldI);
    incrementPC();
}
void chip8::op0x0f29(uint16_t op) {
    // Set I = location of sprite for digit Vx
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    setI(Vx * 0x5);
    incrementPC();
}
void chip8::op0x0f33(uint16_t op) {
    // store the BCD representation of Vx in memory locations I, I+1, I+2
    uint16_t x = (op & 0x0F00) >> 8;
    uint16_t Vx = getV(x);
    uint16_t hundreds = Vx / 100;
    uint16_t tens = (Vx % 100) / 10;
    uint16_t ones = (Vx % 100) % 10;
    setMemory(hundreds,getI());
    setMemory(tens,(getI() + 1));
    setMemory(ones,(getI() + 2));
    incrementPC();
}
void chip8::op0x0f55(uint16_t op) {
    // store registers V0 through Vx in memory starting at addr I
    uint16_t x = (op & 0x0F00) >> 8;
    unsigned char reg = 0;
    uint16_t iaddr = getI();
    for(int u=0;u<=x;u++) {
        reg = getV(u);
        setMemory(reg, iaddr+u);
    }
    incrementPC();
}
void chip8::op0x0f65(uint16_t op) {
    // read registers V0 through Vx from mem location I
    uint16_t x = (op & 0x0F00) >> 8;
    unsigned char reg = 0;
    uint16_t iaddr = getI();
    unsigned char regVal = 0;
    for(int u=0;u<=x;u++) {
        reg = u;
        regVal = getMemory(iaddr+u);
        setV(regVal,u);
    }
    incrementPC();
}
