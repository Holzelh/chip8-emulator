# chip8 C++ emulator

Very simple chip8 interpreter created in C++.

![](trip8Demo.gif)

# Required packages (Linux)

* SDL2 and SDL2_image
* g++

# How to compile and run (Linux)

``` sh
git https://gitlab.com/Holzelh/chip8-emulator.git
```
* cd chip8-emulator
* make
* ./chip8Emu romYouWantToRun

# Notes on bitwise operations

This section will sort of spoil the instructions for you so you should only read it
if you're having trouble with them. Perhaps if you're not particularly grasping how
to do bitwise operations.

* 1nnn
  Opcode example: 0xA2f0
  
  Instruction does: Set I = nnn
  
  What needs to be done: Isolate nnn from the rest of the opcode and set I equal to that.
  
  How its done
  * 1010001011110000 is the binary equivalent of 0xA2f0.
  * &                is the binary AND
  * 0000111111111111 is the binary equivalent of 0x0FFF.
  * 0000001011110000 the above AND operation results in 0x02f0.
  * Now its as easy as I = opcode & 0x0FFF
  The binary AND operation allows you to isolate certain bits when combined with strategically placed 1's.
