/*
** emu.cpp
** Friedrich Hölzel (Herbert M.)
**
** try to understand where I went wrong
*/

#include "chip8.h"
#include "graphics.h"
#include <iostream>
#include <fstream>

extern SDL_Window *gWindow;
extern SDL_Renderer *gRenderer;

int main(int argc, char* argv[]) {
    cout << argv[1] << '\n';
    chip8 chirp = chip8();

    chirp.loadRom(argv[1]);

    //reall emu starts + gfx
    if(!init()) {
        cout << "Failed to init!" << '\n';
    } else {
        //load media
        if(!loadMedia()) {
            cout << "Failed to load media!" << '\n';
        } else {
            //main loop flag
            bool quit = false;

            //Event handler
            SDL_Event e;

            //While app is running
            while(!quit) {
                while(SDL_PollEvent(&e) != 0) {
                    //user requests to quit
                    if(e.type == SDL_QUIT) {
                        quit = true;
                    }
                    else if (e.type == SDL_KEYDOWN) {
                        switch(e.key.keysym.sym) {
                            case SDLK_1: chirp.setKey(1,0x1); break;
                            case SDLK_2: chirp.setKey(1,0x2); break;
                            case SDLK_3: chirp.setKey(1,0x3); break;
                            case SDLK_4: chirp.setKey(1,0xc); break;
                            case SDLK_q: chirp.setKey(1,0x4); break;
                            case SDLK_w: chirp.setKey(1,0x5); break;
                            case SDLK_e: chirp.setKey(1,0x6); break;
                            case SDLK_r: chirp.setKey(1,0xd); break;
                            case SDLK_a: chirp.setKey(1,0x7); break;
                            case SDLK_s: chirp.setKey(1,0x8); break;
                            case SDLK_d: chirp.setKey(1,0x9); break;
                            case SDLK_f: chirp.setKey(1,0xe); break;
                            case SDLK_z: chirp.setKey(1,0xa); break;
                            case SDLK_x: chirp.setKey(1,0x0); break;
                            case SDLK_c: chirp.setKey(1,0xb); break;
                            case SDLK_v: chirp.setKey(1,0xf); break;
                            case SDLK_ESCAPE: exit(1); break;
                            default: cout << "invalid..." << '\n'; break;
                        }
                    }
                    else if (e.type == SDL_KEYUP) {
                        //do somethin
                        switch(e.key.keysym.sym) {
                            case SDLK_1: chirp.setKey(0,0x1); break;
                            case SDLK_2: chirp.setKey(0,0x2); break;
                            case SDLK_3: chirp.setKey(0,0x3); break;
                            case SDLK_4: chirp.setKey(0,0xc); break;
                            case SDLK_q: chirp.setKey(0,0x4); break;
                            case SDLK_w: chirp.setKey(0,0x5); break;
                            case SDLK_e: chirp.setKey(0,0x6); break;
                            case SDLK_r: chirp.setKey(0,0xd); break;
                            case SDLK_a: chirp.setKey(0,0x7); break;
                            case SDLK_s: chirp.setKey(0,0x8); break;
                            case SDLK_d: chirp.setKey(0,0x9); break;
                            case SDLK_f: chirp.setKey(0,0xe); break;
                            case SDLK_z: chirp.setKey(0,0xa); break;
                            case SDLK_x: chirp.setKey(0,0x0); break;
                            case SDLK_c: chirp.setKey(0,0xb); break;
                            case SDLK_v: chirp.setKey(0,0xf); break;
                            default: cout << "invalid..." << '\n'; break;
                        }

                    }
                }
                chirp.emulate();
                if(chirp.getDelay() > 0) {
                    chirp.decrementDelay();
                }
                if(chirp.getSound() > 0) {
                    chirp.decrementSound();
                }
                if(chirp.getDrawf() == true) {
                    for(int i=0;i<64;i++){
                        for(int j=0;j<32;j++){
                            unsigned short di = chirp.getDisp(i, j);
                            unsigned short oldDi = chirp.getOldDisp(i, j);
                            if((di ^ oldDi) == 1) {
                                //rendering
                                SDL_Rect fillRect;
                                fillRect.x = i * SCALE;
                                fillRect.y = j * SCALE;
                                fillRect.w = SCALE;
                                fillRect.h = SCALE;

                                if(chirp.getDisp(i,j) == 1) {
                                    SDL_SetRenderDrawColor(gRenderer, 255, 255, 255, 255);
                                    SDL_RenderFillRect(gRenderer, &fillRect);
                                }
                                if (chirp.getDisp(i,j) == 0) {
                                    SDL_SetRenderDrawColor(gRenderer, 0, 0, 0, 255);
                                    SDL_RenderFillRect(gRenderer, &fillRect);
                                }
                        }   }
                    }
                    SDL_RenderPresent(gRenderer);
                    chirp.setDrawf(false);
                    SDL_Delay(10); //2000
                    //delays
                }
            }
        }
    }

    chirp.~chip8();
    close();
    return 0;
}
