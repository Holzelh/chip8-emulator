/*
** graphics.h
** Friedrich Hölzel (Herbert M.)
**
** handle SDL2 rendering
*/

#include <iostream>
#include <SDL2/SDL.h>
#include <SDL2/SDL_image.h>

const int SCALE = 8;
const int SCREEN_WIDTH = 64 * SCALE;
const int SCREEN_HEIGHT = 32 * SCALE;

//Starts up SDL and creates window
bool init();

//Loads media
bool loadMedia();

//Frees media and shuts down SDL
void close();

//Loads individual image as texture
SDL_Texture* loadTexture( std::string path );
